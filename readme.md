# KVM Switcher Appliance

A specialisation of the 
[Arduino Uno Keyboard template project](https://bitbucket.org/kva1966/arduino-uno-keyboard).

Only tweaks the key sequence application logic. All other gory details can be 
found in the template project.

The KVM handled is a 
[ServerLink/Comsol KVM](http://www.comsol.com.au/downloads/manuals/SL-204-DDL.pdf),
but in essence, any KVM that takes a USB HID input and some control key sequence
will work with the necessary key sequence changes.

Only the very basic, and, very useful primary function of alternating between 
port 1 and 2 is implemented.


# Why?

Some oddities with this KVM when attached to Linux:

* The KVM USB keyboard console detects the keyboard, and any hot keys for 
switching as per the manual.
* However, Linux does not see the keyboard when attached to this console.
* As a workaround, I can attach a keyboard to the auxilliary USB hub on the 
KVM which Linux detects fine, however, then hotkeys are no longer detected by 
the KVM because the keyboard is no longer on the designated console.

Annoyingly, the keyboard is visible under Mac, so some combination of Linux, 
the desktop hardware/BIOS, and the KVM USB console is at fault.

This appliance is a workaround: it will be attached to the KVM keyboard 
console, providing the hotkey functionality. 

The regular keyboard remains attached to the auxilliary USB ports, and works 
fine under both Linux and Mac (the only machines I have tested with).


# Bill of Materials

* Arduino Uno
* 1 pushbutton
* 1 10kOhm resistor
* 1 LED and appropriate resistor


The LED (and hence its "appropriate resistor"!) is optional, I just want a 
visual indicator on button press.

In my case, it's neat that the Arduino is powered by the KVM itself with no 
ill-effect to KVM functionality.

I haven't gotten around to creating a schematic of the circuit (really not sure 
what tool to use yet!), but in words:

* LED connected to pin 12 set as output
* Pushbutton has one terminal connected to pin 2 (set as input), this terminal 
is also connected to ground via the 10k resistor, as shown in 
[State Change Detection Arduino tutorial](https://www.arduino.cc/en/Tutorial/StateChangeDetection)


# Completed Project

Since I wanted this as an appliance, I basically hacked up a container to place 
the circuit. I also decided to try my hand at an Arduino-shield like mechanism 
to place the switch circuit on. This was simply done by soldering the circuit on 
a small perforated board, and adding appropriate headers to connect the relevant
pins.

See the image below (clearly, the plastic cutting needs work...):

![KVM Switch Box](https://bitbucket.org/kva1966/arduino-kvm-switcher/raw/master/images/kvm-switch-box.jpg "KVM Switch Box")

In case you are wondering what the orange bit is -- Scotchbrite sponge strips to give the appliance a snug fit in the container. :-) Scotchbrite is a premium, and reasonably expensive brand, but this is what I had at home (they also last a LOT longer! Well worth the premium cost). In the future, I can stash up on cheapo brands for this purpose. Or maybe bubble wrap, but sponge works better, less noisy, better fit.

And no, I am not a Scotchbrite employee!
